#!/bin/bash

export MOUSEID="$(xinput | awk '/Logitech/ && /pointer/' | awk '{print $6}' | cut -f2 -d"=")"

xinput --set-prop ${MOUSEID} 'Coordinate Transformation Matrix' 1 0 0 0 1 0 0 0 .50


