# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.
alias vi="vim"
alias update_world="emerge -uDU --ask --tree --verbose --deep --changed-use --keep-going --with-bdeps=y @world"
alias install_package="emerge --ask --tree --verbose "
alias update_use="sudo vi /etc/portage/make.conf"
alias sudo="sudo "
alias Grep="grep"
alias mod_song="/home/sp00ky/multimedia/downloads/moosic/command.sh"
#alias startx="startx -- vt1"

complete -cf sudo
set -o vi
[ -z "$STY" ] && screen    # test if $STY is empty
