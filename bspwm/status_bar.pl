#!/usr/local/bin/perl
use strict;
use warnings;

# My delimiter inbetween each arg in lemonbar
my $DELIM="|";
# My special string to identify my partitions in df output
my $HD_IDENTIFIER1="916";
my $HD_IDENTIFIER2="118";

# get the date and year
sub clock {
    `date "+${DELIM} %H:%M:%S $DELIM %m/%d/%Y ${DELIM}"`;
}

# print the avail mem of specified harddrives
# specified harddrives being a key string that identifies each hdd in the df -h output
sub hard_drive_mem {
    my @var = grep(/$HD_IDENTIFIER1|$HD_IDENTIFIER2/, `df -h`);
    my $ret_val = "";
    my @var_args;
    foreach (@var) {
        $_ =~ s/\/dev\///; # remove /dev/
        push (@var_args, split(" ", $_))
    }
    # 6 colums in df, you do the math to find the colums you want
    $ret_val = sprintf("%s %s %s %s %s %s %s",
                       $DELIM,
                       $var_args[0],
                       $var_args[3],
                       $DELIM,
                       $var_args[6],
                       $var_args[9],
                       $DELIM);
    return $ret_val
}

sub network_info {
    my @var = grep(/wlp36s0/, `iwconfig`);
    #my @tmp = split(" ", @var[0]);
    #foreach (@tmp) {
    #    printf("%s\n", $_);
    #printf("%s\n", @var[0]);
    $var[0] =~ /ID:\"(.*)\"/;
    my $tmp = $1;
    #printf("here:%s:here", $tmp);
}

sub get_battery {
    my $bat = "/sys/class/power_supply/BAT0/capacity";
    open(my $fh, '<:encoding(UTF-8)', $bat)
        or die "Could not open file '$bat' $!";

    my $bat_charge = <$fh>;
    chomp $bat_charge;
    return $bat_charge;
}

#testing loop
while (1) {
    #network_info();
    #printf("Here\n");
    #get_battery();
    #sleep 1;
    goto start; 
}

 start:
while(1) {
    my $bar_center = sprintf("%%{c} %s", clock());
    #my $bar_right = sprintf("%%{r} %s", hard_drive_mem());
    my $bar_right = sprintf("%%{r} %s", get_battery());
    #printf("%s %s", $bar_center, $bar_right);

    #printf("%s", $bar_center);
    printf("%s\n", $bar_right);
    sleep 1;
}
