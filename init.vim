filetype on
set autoindent
set cursorline "highlights the current line
"setting ruler and line number
set relativenumber number ruler hidden

"vim-plug
call plug#begin('~/.config/nvim/plugged')

"vim plugins
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-clang'
Plug 'neomake/neomake'
Plug '/nanotech/jellybeans.vim'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-easytags'
Plug 'majutsushi/tagbar'

call plug#end()

"buffer switching
:noremap <C-l> :bnext<CR>
:noremap <C-h> :bprevious<CR>
"buffer closing
nmap <leader>d :bprevious<CR>:bdelete! #<CR>

"Nvim clipboard
set clipboard+=unnamedplus

" CTAG
" vim-easytags
:let g:easytags_file = '/home/gi/multimedia/.vim/vimtags'
"using <f8> to toggle tagbar
nmap <F8> :TagbarToggle<CR>
nmap <F9> :TagbarTogglePause<CR>

"deoplete
let g:deoplete#enable_at_startup = 1

"deoplete
let g:deoplete#sources#clang#libclang_path = '/usr/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/lib/clang'
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
set completeopt-=preview

"neomake
autocmd! BufWritePost,BufEnter * Neomake
let g:neomake_verbose = 1
"cpp
let g:neomake_cpp_enabled_makers = ['clang']
let g:neomake_cpp_clang_maker = {
   \ 'exe': 'clang++',
   \ 'args': ['-Wall', '-Iinclude', '-Wextra', '-pedantic', '-Wno-sign-conversion', '-fsyntax-only'],
   \ }
"NERDTree
let g:NERDTreeDirArrows=0

"vim-airline 
let g:airline#extensions#tabline#enabled = 1

"syntax settings
syntax on
set t_Co=256
colorscheme jellybeans

"tab modifications
set shiftwidth=2
set softtabstop=2
set expandtab

"matching braces
inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}

"mapping upper and lower case commands
:command WQ wq
:command Wq wq
:command W w
:command Q q

"code folding
set foldenable "enable code folding
set foldlevelstart=10 "open folds by default
set foldnestmax=10 "10 nested folds max

"enter a line above current
nmap <S-Enter> O<Esc>

"terminal mode using escape
:tnoremap <Esc> <C-\><C-n>

