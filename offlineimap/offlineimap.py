#! /usr/bin/env python2
from subprocess import check_output

def get_pass(account):
    if account == "gryphmail":
        return check_output("gpg -dq ~/gpg/gryphmail_pass.gpg", shell=True).strip("\n")

    if account == "gmail":
        return check_output("gpg -dq ~/gpg/gmail_pass.gpg", shell=True).strip("\n")
